# pulsar-go-sample

## Go version

``` shell
$ go version
go version go1.12.5 darwin/amd64
```

## Init

``` shell
$ GO111MODULE=on go mod init
```

## yaml

``` shell
$ GO111MODULE=on go build yaml/yaml.go
$ GO111MODULE=on go run yaml/yaml.go
# or
$ go run yaml/yaml.go
```

## Server

``` shell
$ docker run -it --rm \
  -p 6650:6650 \
  -p 8080:8080 \
  -v $PWD/data:/pulsar/data \
  apachepulsar/pulsar:2.3.1 \
  bin/pulsar standalone
```

## Client

### Producer

``` shell
$ GO111MODULE=on go build producer/producer.go
$ GO111MODULE=on go run producer/producer.go
# or
$ go run producer/producer.go
# comcast
$ GO111MODULE=on go run producer/comcast-producer.go
```


### Consumer

``` shell
$ GO111MODULE=on go build consumer/consumer.go
$ GO111MODULE=on go run consumer/consumer.go
# or
$ go run consumer/consumer.go
# comcast
$ GO111MODULE=on go run consumer/comcast-consumer.go
```



EOF
