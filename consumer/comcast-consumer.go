package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"os"
	"time"

	"github.com/Comcast/pulsar-client-go"
)

func main() {
	asyncErrs := make(chan error, 8)
	var tlsCfg *tls.Config
	mcp := pulsar.NewManagedClientPool()
	queue := make(chan pulsar.Message, 8)

	// Create managed consumer
	mcCfg := pulsar.ManagedConsumerConfig{
		Name:                  "demo",
		Topic:                 "persistent://sample/standalone/ns1/demo",
		Exclusive:             !false,
		NewConsumerTimeout:    time.Second,
		InitialReconnectDelay: time.Second,
		MaxReconnectDelay:     time.Minute,
		ManagedClientConfig: pulsar.ManagedClientConfig{
			ClientConfig: pulsar.ClientConfig{
				Addr:      "localhost:6650",
				TLSConfig: tlsCfg,
				Errs:      asyncErrs,
			},
		},
	}
	ctx, _ := context.WithCancel(context.Background())
	mc := pulsar.NewManagedConsumer(mcp, mcCfg)
	go mc.ReceiveAsync(ctx, queue)
	fmt.Printf("Created consumer %q on topic %q...\n", "demo", "persistent://sample/standalone/ns1/demo")

	for {
		select {
		case <-ctx.Done():
			return

		case msg := <-queue:
			fmt.Println(string(msg.Payload))
			if err := mc.Ack(ctx, msg); err != nil {
				fmt.Fprintf(os.Stderr, "error acking message: %v", err)
			}
		}
	}
}
