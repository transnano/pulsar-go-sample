package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"os"
	"time"

	"github.com/Comcast/pulsar-client-go"
)

func main() {
	asyncErrs := make(chan error, 8)
	var tlsCfg *tls.Config
	mcp := pulsar.NewManagedClientPool()
	// Create the managed producer
	mpCfg := pulsar.ManagedProducerConfig{
		Name:                  "demo",
		Topic:                 "persistent://sample/standalone/ns1/demo",
		NewProducerTimeout:    time.Second,
		InitialReconnectDelay: time.Second,
		MaxReconnectDelay:     time.Minute,
		ManagedClientConfig: pulsar.ManagedClientConfig{
			ClientConfig: pulsar.ClientConfig{
				Addr:      "localhost:6650",
				TLSConfig: tlsCfg,
				Errs:      asyncErrs,
			},
		},
	}
	mp := pulsar.NewManagedProducer(mcp, mpCfg)
	fmt.Printf("Created producer on topic %q...\n", "persistent://sample/standalone/ns1/demo")

	// messages to produce are sent to this
	// channel
	messages := make(chan []byte)

	go func() {
		var i int
		i++
		messages <- []byte(fmt.Sprintf("%03d %s", i, "MessageXXX"))
	}()

	ctx, _ := context.WithCancel(context.Background())

	for {
		select {
		case payload, ok := <-messages:
			if !ok {
				return
			}
			sctx, cancel := context.WithTimeout(ctx, time.Second)
			_, err := mp.Send(sctx, payload)
			cancel()
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				continue
			}

		case <-ctx.Done():
			return
		}
	}
}
