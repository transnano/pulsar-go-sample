package main

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type Event struct {
	// gorm.Model
	Id   int    `gorm:"primary_key" "column:id"`
	Name string `gorm:"column:name"`
}

func main() {
	db := initMigrate()
	defer db.Close()
	run(db)
}

func initMigrate() *gorm.DB {
	db, err := gorm.Open("mysql", "root:test@tcp(127.0.0.1:3306)/test?parseTime=true")
	if err != nil {
		log.Panic(err)
	}

	db.AutoMigrate(&Event{})
	db.LogMode(true)
	return db
}

func run(db *gorm.DB) {
	var event Event
	db.First(&event, "id = ?", 100)
	if db.Error != nil {
		panic(db.Error)
	}
	fmt.Printf("%d: %v", event.Id, event.Name)
}
